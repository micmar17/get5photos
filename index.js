
var axios = require('axios');
var wget = require('node-wget');

var apiKey = 'e5ea793f46ffb7da4b4d62bfed8279b783c8e97577bb1c0dc01f5c2e1ca01e31';
var firstUrl = 'https://api.unsplash.com/';
var secondUrl = 'photos/random/';


axios
    .get(firstUrl + secondUrl,
        {
            params:
            {
                client_id: apiKey,
                count: 5
            }
        })
    .then(function (response) 
    {
        for (var i = 0; i < 5; i++) 
            {
                wget(
                    {
                        url: response.data[i].urls.full,
                        dest: './photo' + (i).toString() + '.jpg'
                    },
                    function (error) 
                    {
                        if (error)
                            console.log(error);
                    });
            }
        })
    .catch(
        function (error) {
            if (error)
                console.log(error);
        }
    );
